import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import FormTodo from './FormTodo.js'

class ModalTodo extends React.Component {

  render() {
    return (
      <>
        <Modal show={this.props.modalShow} onHide={() => this.props.OpenModal(this.props.isNew, this.props.index)}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.isNew ? "Adicionar nova tarefa" : "Editar tarefa"}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormTodo
              OnSubmit = {this.props.OnSubmit}
              HandleChangeOnForm = {this.props.HandleChangeOnForm}
              currentTodo = {this.props.currentTodo}
              isNew = {this.props.isNew}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="outline-secundary" onClick={() => this.props.OpenModal(this.props.isNew, this.props.index)}>Fechar</Button>
            <Button variant="outline-dark" type="submit" form="formTodo">{this.props.isNew ? "Adicionar" : "Editar"}</Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ModalTodo
