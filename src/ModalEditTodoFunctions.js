
export function HandleEditTodo(todo, event) {
  //altera o valor que está sendo editado. Selecionamos esse todo usando o index que foi passado como argumento para função
  if(event.target.name == 'tags') {
    //Caso as tags estejam sendo alteradas ele irá sepra-las pela vírgula
    todo[event.target.name] = event.target.value.split(',');
  } else if (event.target.name == 'date') {
    todo['dateFinal'][0] = event.target.value;
  } else if(event.target.name == 'time') {
    todo['dateFinal'][1] = event.target.value;
  } else {
    todo[event.target.name] = event.target.value;
  }
  return todo
}
