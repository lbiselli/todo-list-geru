import React, {Component} from 'react';

import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';



class FormTodo extends React.Component {

  render() {

    return (
      <Form id="formTodo" onSubmit={(e) => this.props.OnSubmit(e, this.props.isNew)}>
        <Form.Group controlId="formNewTodoDescription">
          <Form.Label>Descrição da Tarefa</Form.Label>
          <Form.Control required type="text" placeholder="Descrição" name="description" onChange={(e) => this.props.HandleChangeOnForm(e)} defaultValue={this.props.currentTodo['description']}/>
        </Form.Group>

        <Form.Row>
          <Form.Group as={Col} controlId="formNewTodoDate">
            <Form.Label>Data do Evento</Form.Label>
            <Form.Control required type="date" placeholder="Date" name="date" onChange={(e) => this.props.HandleChangeOnForm(e)} defaultValue={this.props.currentTodo.dateFinal[0]}/>
          </Form.Group>
          <Form.Group as={Col} controlId="formNewTodoTime">
            <Form.Label>Horário do Evento</Form.Label>
            <Form.Control required type="time" placeholder="Time" name="time" onChange={(e) => this.props.HandleChangeOnForm(e)} defaultValue={this.props.currentTodo.dateFinal[1]}/>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group as={Col} controlId="formNewTodoDuration">
            <Form.Label>Previsão de Duração</Form.Label>
            <Form.Control required type="number" placeholder="Duração" name="duration" onChange={(e) => this.props.HandleChangeOnForm(e)} defaultValue={this.props.currentTodo['duration']} min={0}/>
            <Form.Text className="text-muted">
              Em minutos
            </Form.Text>
          </Form.Group>
          <Form.Group as={Col} controlId="formNewTodoReminder">
            <Form.Label>Tempo para Lembrete</Form.Label>
            <Form.Control required type="number" placeholder="Lembrete" name="reminder" onChange={(e) => this.props.HandleChangeOnForm(e)} defaultValue={this.props.currentTodo['reminder']}/>
            <Form.Text className="text-muted">
              Em minutos
            </Form.Text>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group as={Col} controlId="formNewTodoTags">
            <Form.Label>Tags</Form.Label>
            <Form.Control type="text" placeholder="Tags" name="tags" onChange={(e) => this.props.HandleChangeOnForm(e)} defaultValue={this.props.currentTodo.tags}/>
            <Form.Text className="text-muted">
              Separe as Tags por vírgula
            </Form.Text>
          </Form.Group>
        </Form.Row>

      </Form>
    )
  }

}

export default FormTodo;
