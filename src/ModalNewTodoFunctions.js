export function parseDataForSubmit(currentTodo) {

  //transforma a data de criação do todo em uma timestamp
  currentTodo.dateOfCreation = parseInt(Date.now()/1000);

  //torna tanto duration quanto reminder em ints, uma vez que elas estão sendo guardadas como int no servidor
  currentTodo.duration = parseInt(currentTodo.duration);
  currentTodo.reminder = parseInt(currentTodo.reminder);

  return currentTodo
}

//Essa função serve para "limpar" o currentTodo do setState
//Ela foi criada, pois deixa o código mais limpo
export function ClearedCurrentTodo() {
  const currentTodo = {
    id: '',
    description: '',
    dateFinal: [],
    duration: '',
    reminder: '',
    dateOfCreation: '',
    done: false,
    tags: []
  };
  return currentTodo
}

export function ChangeCurrentTodo(currentTodo, event) {
  //caso esteja colocando tags temos que separa-las por espaço e colocarmos no array
  if(event.target.name == 'tags') {
    //cria um vetor de tags seperando elas por vírgula
    currentTodo[event.target.name] = event.target.value.split(',');
  } else if(event.target.name == 'date') {
    currentTodo['dateFinal'][0] = event.target.value;
  } else if(event.target.name == 'time') {
    currentTodo['dateFinal'][1] = event.target.value;
  } else {
    //altera o estado apenas da caracteristica alterada
    currentTodo[event.target.name] = event.target.value;
  }
  return currentTodo;
}
