import React, { Component } from 'react';
import logo from './logo.svg';

import './App.css';
import TodoListHeader from './TodoListHeader';
import TodoItem from './TodoItem';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

import axios from 'axios';

import { parseDataForSubmit, ClearedCurrentTodo, ChangeCurrentTodo } from './ModalNewTodoFunctions';
import { UploadNewData, UploadEditedData, UploadChangeOnDone, DeleteData } from './HandleData';
import { HandleEditTodo } from './ModalEditTodoFunctions';

class App extends Component {

  constructor() {
    super();

    //Funções para modal e adição de novos todos

    //Funções para deletar todos
    this.cardButtonDeleteTodo = this.CardButtonDeleteTodo.bind(this);

    //Funções para tornar tarefas done
    this.DoneTask = this.DoneTask.bind(this);

    this.HandleChangeOnForm = this.HandleChangeOnForm.bind(this);
    this.OpenModal = this.OpenModal.bind(this);
    this.OnSubmit = this.OnSubmit.bind(this);

    //Funções para editar tarefas
    this.SetCurrentTodo = this.SetCurrentTodo.bind(this);

    //Funções para mudar "visualização" (todos, realizados, não realizados)
    this.SetShowing = this.SetShowing.bind(this);

    this.state = {
      todos: [],
      currentTodo: {
        id: '',
        description: '',
        dateFinal: [],
        duration: '',
        reminder: '',
        dateOfCreation: '',
        done: false,
        tags: []
      },
      modalShowNew: false,
      modalShowEdit: false,
      //A ideia é que 2 seja todo, 0 realizados e 1 não realizados
      showing: 2,
    };
  }

  componentDidMount() {
    //assim que o componet terminar de carregar nos iremos carregar as informações que estão no servidor
    axios.get('https://5c8e5cae3e557700145e84ef.mockapi.io/api/todos').then(res => {
      //para cada todo no vetor que ele retornar eu quero que ele crie um todo
      let todos = res.data.map((todo) => {
        let todoForState = {};

        todoForState.id = parseInt(todo.id);
        todoForState.description = todo.description;
        todoForState.dateFinal = todo.dateFinal;
        todoForState.duration = todo.duration;
        todoForState.reminder = todo.reminder;
        todoForState.done = todo.done;
        todoForState.tags = todo.tags;

        return todoForState;
      })
      //Coloca esses todos baixados no state
      this.setState({todos: todos});
    });
  }

  //----------FUNÇÕES PARA O MODAL DE NOVOS TODOS--------------------//
  //-------------------------COMEÇO----------------------------------//

  //Submit da form para novos todos
  OnSubmitNew = (e) => {
    e.preventDefault();
    //coloco a data de criaçao no currentTodo
    const currentTodo = parseDataForSubmit(this.state.currentTodo);
    currentTodo['id'] = parseInt(this.state.todos.length+1);
    //Adiciona o Item a lista de atividades
    //reseta o currentTodo se não todo novo todo na verdade vai alterar o ultimo todo
    const todos = [...this.state.todos, currentTodo];
    //faz o upload do novo todo para o servidor
    UploadNewData(currentTodo);
    //set o state dos todos e limp o state do currentTodo usando essa função para deixar o código mais limpo
    this.setState({todos: todos, currentTodo: ClearedCurrentTodo()});
    //Tira o Modal
    this.OpenModal(true, 0);
  }
  //-------------------------FIM----------------------------------//
  //----------FUNÇÕES PARA O MODAL DE NOVOS TODOS-----------------//

  //Toda mudança de dados no Modal para adicionar tarefas será tratado por aqui
  HandleChangeOnForm = event => {
    //Colocamos essa função para deixar o código mais legível.
    const currentTodo = ChangeCurrentTodo(this.state.currentTodo, event);
    this.setState({currentTodo: currentTodo});
  }

  OpenModal(isNew, index) {
    console.log(isNew);
    console.log(index);
    if (isNew) {
      //altera o state show para o contrário do que ele é no momento
      //ou seja se ele estiver aberto (true) ele fechará (false)
      const modalShowNew = !this.state.modalShowNew;
      this.setState({ modalShowNew: modalShowNew });
    } else {
      //altera o state show para o contrário do que ele é no momento
      //ou seja se ele estiver aberto (true) ele fechará (false)
      if(!this.state.modalShowEdit) {
        this.setState({ modalShowEdit: true });
        //se estivermos abrindo o modal de edição quer dizer que um todo será editado. Então carregamos esse todo para o state currentTodo
        //para podermos altera-lo
        this.SetCurrentTodo(index);
      } else {
        //se fecharmos a edição de um todo não iremos necessitar que ele esteja no carregado no state currentTodo
        //Em função disso, limpamos o currentTodo
        this.setState({ modalShowEdit: false, currentTodo: ClearedCurrentTodo()});
      }
    }
  }

  //roteia o onSubmit do form dependendo se é um novo todo ou não
  OnSubmit = (e, isNew) => {
    if (isNew) {
      this.OnSubmitNew(e);
    } else {
      this.OnSubmitEdit(e);
    }
  }


  //----------FUNÇÕES PARA O MODAL DE EDITAR TODOS--------------------//
  //-------------------------COMEÇO----------------------------------//

  OnSubmitEdit = (e) => {
    e.preventDefault();

    const todos = this.state.todos
    const currentTodo = this.state.currentTodo;
    todos[currentTodo['id']-1] = currentTodo;

    this.setState({todos: todos});
    UploadEditedData(currentTodo);
    this.OpenModal(false, 0);
  }

  //-------------------------FIM------------------------------------//
  //----------FUNÇÕES PARA O MODAL DE EDITAR TODOS-----------------//




  CardButtonDeleteTodo = index => {
    //"pega" o array que esta no state
    let todos = this.state.todos;
    //remove apenas o todo do index
    todos.splice(index, 1);
    //atualiza o state
    this.setState({todos: todos});
    DeleteData(index+1)

  }

  //Função que trata de tornar os todos done ou não
  DoneTask = index => {
    const todos = Object.assign({}, this.state.todos);
    todos[index]['done'] = !todos[index]['done'];

    this.setState({todos: todos});
    UploadChangeOnDone(index+1, todos[index]['done']);
  }

  SetCurrentTodo = index => {
    const currentTodo = Object.assign({}, this.state.todos[index]);
    this.setState({currentTodo: currentTodo});
  }

  SetShowing(event) {
    switch (event) {
      case 0:
        this.setState({showing: 0});
        break;
      case 1:
        this.setState({showing: 1});
        break;
      default:
        this.setState({showing: 2});
    }
  }

  render() {
    return (
      <div className="App">
        <TodoListHeader
          currentTodo = {this.state.currentTodo}
          modalShowNew = {this.state.modalShowNew}
          modalShowEdit = {this.state.modalShowEdit}
          showing = {this.state.showing}

          SetShowing = {this.SetShowing}
          OpenModal = {this.OpenModal}
          HandleChangeOnForm = {this.HandleChangeOnForm}
          OnSubmit = {this.OnSubmit}
        />
        <Container>
          <Row className="todos">
            <Col md={{ span: 8, offset: 2 }}>
            </Col>
          </Row>
          <Row className="todos">
            <Col md={{ span: 8, offset: 2 }}>
              <TodoItem
                todos= {this.state.todos}
                CardButtonDeleteTodo = {this.CardButtonDeleteTodo}
                DoneTask = {this.DoneTask}
                currentTodo = {this.state.currentTodo}
                showing = {this.state.showing}

                modalShowNew = {this.state.modalShowNew}
                modalShowEdit = {this.state.modalShowEdit}
                OpenModal = {this.OpenModal}
                HandleChangeOnForm = {this.HandleChangeOnForm}
                OnSubmit = {this.OnSubmit}
                SetCurrentTodo = {this.SetCurrentTodo}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
