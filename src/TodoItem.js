import React, { Component } from 'react';

import Card from 'react-bootstrap/Card';
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Badge from 'react-bootstrap/Badge';

import ModalTodo from './ModalTodo';

class TodoItem extends Component {

  SetShowing() {

    let todoShowing = []

    switch (this.props.showing) {
      //0 é igual a mostrar os todos done
      case 0:
        todoShowing = this.props.todos.filter((todo) => {
          return todo.done == true;
        });
        break;
      //1 é igual a mostrar os todos not done
      case 1:
        todoShowing = this.props.todos.filter((todo) => {
          return todo.done == false;
        });
        break;
      //o default é mostrar todos
      default:
        todoShowing = this.props.todos;
    };

    return todoShowing;
  }

  render() {

    //sera o array de cards que serão mostrados
    let todoShowing = this.SetShowing();

    //seria um for each em cada item que esta em todo
    const listOfTodos = todoShowing.map((todo, index) => {

      let done = false;

      if (todo['done']) {
        done = true;
      }

      //para cada tag ele cria um "Badge" que sera exibido ao lado do titulo
      let tags = todo.tags.map(tag => {
        return <Badge variant="dark" className="tagsTodoItem">{tag}</Badge>
      });
      return (
        <Card className="todoCards">
          <InputGroup className="mb">
            <InputGroup.Prepend>
              <InputGroup.Checkbox aria-label="Checkbox for following text input" onClick={() => this.props.DoneTask(index)} checked={done ? true : false}/>
            </InputGroup.Prepend>
            <Card.Body>
              <Card.Title className="cardsBody" style={{textDecoration: done ? 'line-through' : 'none'}}>{todo['description']}{tags}</Card.Title>
              <Card.Subtitle className="text-muted cardsBody" style={{textDecoration: done ? 'line-through' : 'none'}}>Data: {todo.dateFinal[0] + ' as ' + todo.dateFinal[1]}</Card.Subtitle>
            </Card.Body>
            <InputGroup.Append>
              <ButtonGroup vertical>
                <Button variant="outline-secondary" onClick={() => this.props.OpenModal(false, index)} className="cardButton">Editar</Button>
                <ModalTodo
                  modalShow = {this.props.modalShowEdit}
                  OpenModal = {this.props.OpenModal}
                  HandleChangeOnForm = {this.props.HandleChangeOnForm}
                  OnSubmit = {this.props.OnSubmit}
                  index = {index}
                  currentTodo = {this.props.currentTodo}
                  isNew = {false}
                />
                <Button variant="outline-danger" className="cardButton" id="cardButtonDelete" onClick={() => this.props.CardButtonDeleteTodo(index)}>Deletar</Button>
              </ButtonGroup>
            </InputGroup.Append>
          </InputGroup>
        </Card>
    )});

    return listOfTodos;
  }

}

export default TodoItem
