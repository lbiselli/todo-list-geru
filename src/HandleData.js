import axios from 'axios';

export function UploadNewData(todo) {

  axios.post('https://5c8e5cae3e557700145e84ef.mockapi.io/api/todos', {
    description: todo.description,
    dateFinal: todo.dateFinal,
    duration: todo.duration,
    reminder: todo.reminder,
    dateOfCreation: todo.dateOfCreation,
    tags: todo.tags,
    done: todo.done
  })
    .then(res => {
      console.log(res);
      console.log(res.data);
    })
}

export function UploadEditedData(todo) {

  axios.put('https://5c8e5cae3e557700145e84ef.mockapi.io/api/todos/'+todo.id, {
    description: todo.description,
    dateFinal: todo.dateFinal,
    duration: todo.duration,
    reminder: todo.reminder,
    dateOfCreation: todo.dateOfCreation,
    tags: todo.tags,
    done: todo.done
  })
    .then(res => {
      console.log(res);
      console.log(res.data);
    }).catch(res =>{
      console.log(res);
    })
}

export function UploadChangeOnDone(id, done){
  axios.put('https://5c8e5cae3e557700145e84ef.mockapi.io/api/todos/'+id, {
    done: done
  })
    .then(res => {
      console.log(res);
      console.log(res.data);
    }).catch(res =>{
      console.log(res);
    })
}

export function DeleteData(id){
  axios.delete('https://5c8e5cae3e557700145e84ef.mockapi.io/api/todos/'+id)
    .then(res => {
      console.log(res);
      console.log(res.data);
    }).catch(res =>{
      console.log(res);
    })
}
