import React, {Component} from 'react';

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import ModalTodo from './ModalTodo';
import TodoItem from './TodoItem';

class TodoListHeader extends Component {

  render() {
    return (
      <div className="todoListMain">
        <div className="header">

          <Navbar bg="dark" variant="dark">
            <Navbar.Brand>Todo List</Navbar.Brand>
            <Nav className="mr-auto">
              <Nav.Link onClick={() => this.props.SetShowing(2)} className={this.props.showing === 2 ? "active" : ""}>Todos</Nav.Link>
              <Nav.Link onClick={() => this.props.SetShowing(0)} className={this.props.showing === 0 ? "active" : ""}>Realizadas</Nav.Link>
              <Nav.Link onClick={() => this.props.SetShowing(1)} className={this.props.showing === 1 ? "active" : ""}>Não Realizadas</Nav.Link>
            </Nav>
            <Button variant="outline-light" onClick={() => this.props.OpenModal(true, this.props.currentTodo['id'])} className="todoAddNewButton">Adicionar nova tarefa</Button>
            <ModalTodo
              currentTodo = {this.props.currentTodo}
              OnSubmit = {this.props.OnSubmit}
              index = {this.props.currentTodo['id']-1}
              isNew = {true}
              OpenModal = {this.props.OpenModal}
              modalShow = {this.props.modalShowNew}
              HandleChangeOnForm = {this.props.HandleChangeOnForm}
            />
          </Navbar>
        </div>
      </div>
    )
  }
}

export default TodoListHeader;
